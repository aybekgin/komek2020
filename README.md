
*Read this in other languages: [EN](README.md), [RU](README.ru.md).*

# Komek2020

This project is developing a platform for the exchange of medicines, food and help. This project arose during the СOVID-19 pandemic to solve its mission of allowing people to help each other. 

The development is carried out by a group of enthusiasts and volunteers. Everyone can contribute, there are many ways in which you can do it:
* Coding, sure
* UI/UX, Design 
* DevOps & DBA
* Analytics, PM
* Copywriting
* any other help you can suggest - from text proofreading to good advice.

All of them are very important and everyone is welcome to participate.

![License AGPL](https://img.shields.io/badge/License-AGPL-blueviolet)

## How to join the project

To get started you can:
* [Join the Telegram discussion group](https://t.me/solve_komek2020)
* [See our tasks on the Trello board](https://trello.com/b/JVbFraxx/komek2020)
* [Join the Slack development team](https://join.slack.com/t/komek2020/shared_invite/zt-fjcnn87r-YIt5hYFaMTS7dFSGo_WrMA)
* Fork this project and evolve it in your country

If you have any ideas how else you can make this project better - please let us know.

### Documentation

You can find all our documentation in the [doc](doc).

### How to contribute

Ready to contribute? Great! First, read our [CONTRIBUTING.md](CONTRIBUTING.md).  
