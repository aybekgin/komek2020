package com.hax0rs.meds.service;


import com.hax0rs.meds.model.dto.MedicationOfferDTO;
import com.hax0rs.meds.model.dto.MedicationRequestDTO;
import com.hax0rs.meds.model.dto.PageFilterDTO;
import com.hax0rs.meds.model.entity.MedicationOffer;
import com.hax0rs.meds.model.entity.MedicationRequest;
import com.hax0rs.meds.repository.MedicationRequestRepository;
import com.hax0rs.meds.repository.MedicationOfferRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class MedsService {

    private final MedicationOfferRepository medicationOfferRepository;
    private final MedicationRequestRepository medicationRequestRepository;


    public void submitRequest(MedicationRequestDTO medicationRequestDTO) {
        MedicationRequest medicationRequest = MedicationRequest.builder()
                .city(medicationRequestDTO.getCity())
                .mobile(medicationRequestDTO.getMobile())
                .name(medicationRequestDTO.getName())
                .requirement(medicationRequestDTO.getRequirement())
                .createdAt(LocalDateTime.now())
                .build();
        medicationRequestRepository.save(medicationRequest);
    }


    public void submitOffer(MedicationOfferDTO medicationOfferDTO) {
        MedicationOffer medicationRequest = MedicationOffer.builder()
                .city(medicationOfferDTO.getCity())
                .mobile(medicationOfferDTO.getMobile())
                .name(medicationOfferDTO.getName())
                .offer(medicationOfferDTO.getOffer())
                .createdAt(LocalDateTime.now())
                .build();
        medicationOfferRepository.save(medicationRequest);
    }

    private Pageable getPageAble(PageFilterDTO pageFilter) {
        Sort sort = null;
        if (pageFilter.getSortBy() != null) {
            sort = Sort.by(pageFilter.getSortDirection(), pageFilter.getSortBy().toString());
        } else {
            sort = Sort.unsorted();
        }
        return PageRequest.of(pageFilter.getPage() - 1, pageFilter.getPageSize(), sort);
    }

    public Page<MedicationOffer> getOffers(PageFilterDTO pageFilter) {

        Pageable pageable = getPageAble(pageFilter);
        if (pageFilter.getQuery() != null && pageFilter.getCity() == null) {
            return medicationOfferRepository.findByOfferContainingIgnoreCase(pageFilter.getQuery(), pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() == null) {
            return medicationOfferRepository.findByCityContainingIgnoreCase(pageFilter.getCity(), pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() != null) {
            return medicationOfferRepository.findByCityContainingIgnoreCaseAndOfferContainingIgnoreCase(pageFilter.getCity(), pageFilter.getQuery(), pageable);
        }
        return medicationOfferRepository.findAll(pageable);
    }

    @Transactional
    public void deleteOffer(String mobile) {
        medicationOfferRepository.deleteByMobileContainingIgnoreCase(mobile);
    }

    @Transactional
    public void deleteRequest(String mobile) {
        medicationRequestRepository.deleteByMobileContainingIgnoreCase(mobile);
    }


    public Page<MedicationRequest> getRequests(PageFilterDTO pageFilter) {
        Pageable pageable = getPageAble(pageFilter);
        if (pageFilter.getQuery() != null && pageFilter.getCity() == null) {
            return medicationRequestRepository.findByRequirementContainingIgnoreCase(pageFilter.getQuery(), pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() == null) {
            return medicationRequestRepository.findByCityContainingIgnoreCase(pageFilter.getCity(), pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() != null) {
            return medicationRequestRepository.findByCityContainingIgnoreCaseAndRequirementContainingIgnoreCase(pageFilter.getCity(), pageFilter.getQuery(), pageable);
        }
        return medicationRequestRepository.findAll(pageable);
    }
}
