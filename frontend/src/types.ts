export type MedicationOffer = {
  city: string;
  mobile: string;
  name: string;
  offer: string;
  createdAt?: string;
};

export type MedicationOfferItem = {
  city: string;
  id: number;
  mobile: string;
  name: string;
  offer: string;
  createdAt: string;
};

export type MedicationRequest = {
  city: string;
  mobile: string;
  name: string;
  requirement: string;
};

export type MedicationRequestItem = {
  city: string;
  id: number;
  mobile: string;
  name: string;
  requirement: string;
  createdAt: string;
};

export type Sort = {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
};

export type Pageable = {
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  sort: Sort;
  unpaged: boolean;
};

export type MedicationOfferPage = {
  content: MedicationOffer[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: Pageable;
  size: number;
  sort: Sort;
  totalElements: number;
  totalPages: number;
};

export type MedicationRequestPage = {
  content: MedicationRequest;
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: Pageable;
  size: number;
  sort: Sort;
  totalElements: number;
  totalPages: number;
};
